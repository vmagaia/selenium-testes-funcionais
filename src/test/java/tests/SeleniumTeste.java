package tests;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

import javax.swing.*;

public class SeleniumTeste {

    private static WebDriver driver;


    @BeforeAll
    public static void setUp(){

        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        driver = new ChromeDriver(options);

    }


    @AfterAll
    public static void tearDown(){
        driver.quit();
    }


    @Test
    public void testTituloValido() throws InterruptedException {

        driver.get("https://www.bci.co.mz/");

        Assertions.assertEquals("BCI - O Meu Banco", this.driver.getTitle());

        //System.out.println("O título da página é: " + driver.getTitle());
    }



    @Test
    public void testLogoIsDisplayed() throws InterruptedException {

        driver.get("https://www.emprego.co.mz/");

        WebElement elem = driver.findElement(By.cssSelector(".header-container>h1>.logo"));
        Assertions.assertTrue(elem.isDisplayed());
    }



    @Test
    //@Epic("Menú")
    //@Feature("Menú")
    //@Story("Elementos clicavéis em toda li")
    public void testMenuDropdown() throws InterruptedException {

        driver.get("https://www.contact.co.mz/pt/");
        WebElement menu = driver.findElement(By.cssSelector("li[data-codigo='empresa']"));

        Actions action = new Actions(driver);
        action.moveToElement(menu).build().perform();//MouseOver of Menu

        WebElement subMenu = driver.findElement(By.cssSelector("li[data-codigo='empresa.estrutura-de-gestao']"));
        action.moveToElement(subMenu).build().perform();//MouseOver of Sub-Menu
        subMenu.click();

    }



}
